/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteStateless;

import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Remote;
import paqueteEntidad.Libro;

/**
 *
 * @author ALEXANDER
 */
@Remote
public interface LibroBeanRemote {
    public void addLibro(String titulo, String autor, BigDecimal precio);
    Collection <Libro> getAllLibros(); 
    public Libro buscaLibro(int id);
    public void actualizaLibro(Libro libro, String Titulo,  String autor, BigDecimal precio);
    public void eliminaLibro(int id);
}
