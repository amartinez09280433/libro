/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paqueteStateless;

import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import paqueteEntidad.Libro;

/**
 *
 * @author ALEXANDER
 */
@Stateless
public class LibroBean implements LibroBeanRemote {

    @PersistenceContext(name = "LibreriaEnterpriseEdition-ejbPU")
    EntityManager em;
    Libro libro;
    Collection<Libro> listaLibros;

    public void addLibro(String titulo, String autor, BigDecimal precio) {
        if (libro == null) {
            libro = new Libro(titulo, autor, precio); //mandamos los datos del libro
            em.persist(libro); //agregamos el libro
            libro = null; //liberamos el libro
        }
    }

    public Collection<Libro> getAllLibros() {
        listaLibros = em.createNamedQuery("Libro.findAll").getResultList();
        return listaLibros;
    }

    @Override
    public Libro buscaLibro(int id) {
        libro = em.find(Libro.class, id);
        return libro;
    }

    @Override
    public void actualizaLibro(Libro libro, String Titulo, String autor, BigDecimal precio) {
        if (libro != null) {
            libro.setTitulo(Titulo);
            libro.setAutor(autor);
            libro.setPrecio(precio);
            em.merge(libro); //El merge es para actualizar
        }
    }

    @Override
    public void eliminaLibro(int id) {
        libro=em.find(Libro.class, id);
        if (libro != null) {
            em.remove(libro);
            libro=null;
        }
    }

}
