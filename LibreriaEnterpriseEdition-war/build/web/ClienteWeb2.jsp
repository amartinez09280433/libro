<%@page import="paqueteEntidad.*,paqueteStateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
    private LibroBeanRemote librocat = null;
    String s0, s1, s2, s3;
    Collection list;
    Libro libro;

    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando catalogo" + librocat);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>
<%
    try {
        s0 = request.getParameter("id");
        s1 = request.getParameter("t1");
        s2 = request.getParameter("aut");
        s3 = request.getParameter("precio");

        if (s0 != null && s1 != null && s2 != null && s3 != null) {
            Integer id = new Integer(s0);
            Double precio = new Double(s3);
            BigDecimal b = new BigDecimal(precio);
            libro=librocat.buscaLibro(id.intValue());
            librocat.actualizaLibro(libro, s1, s2, b);
            System.out.println("Registro actualizado ");
%>
<p>
    <b>
        Registro Actualizado
    </b>
</p>
<%
    }
    list = librocat.getAllLibros();
    for (Iterator iter=list.iterator(); iter.hasNext();) {
        Libro elemento = (Libro) iter.next();
%>
<br/>
<p>ID: <b><%= elemento.getId()%> </b></p>
<p>Titulo: <b><%= elemento.getTitulo()%> </b></p>
<p>Autor: <b> <%= elemento.getAutor()%></b></p>
<p>Precio: <b> <%= elemento.getPrecio()%> </b></p>
<%
    }
    response.flushBuffer();
%>
<h3>Regresar a <a href="buscaId.jsp">forma</a></h3>
<%
    } catch (Exception b) {
        b.printStackTrace();
    }
%>
