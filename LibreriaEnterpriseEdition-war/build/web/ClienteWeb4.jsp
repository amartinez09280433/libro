<%@page import="paqueteEntidad.*,paqueteStateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
    private LibroBeanRemote librocat = null;
    String s0, s1, s2, s3;
    Collection list;
    Libro libro;

    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando catalogo" + librocat);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>
<%
    try {
        s0 = request.getParameter("id");
        if (s0 != null) {
            Integer id = new Integer(s0);
            librocat.eliminaLibro(id);
            System.out.println("Libro eliminado");
%>
<p>
    <b>
        Libro eliminado
    </b>
</p>
<%
    }
    list = librocat.getAllLibros();
    for (Iterator iter=list.iterator(); iter.hasNext();) {
        Libro elemento = (Libro) iter.next();
%>
<br/>
<p>ID: <b><%= elemento.getId()%> </b></p>
<p>Titulo: <b><%= elemento.getTitulo()%> </b></p>
<p>Autor: <b> <%= elemento.getAutor()%></b></p>
<p>Precio: <b> <%= elemento.getPrecio()%> </b></p>
<%
    }
    response.flushBuffer();
%>
<h3>Regresar a <a href="buscaId.jsp">forma</a></h3>
<%
    } catch (Exception b) {
        b.printStackTrace();
    }
%>
