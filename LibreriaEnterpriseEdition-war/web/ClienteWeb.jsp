<%@page import="paqueteEntidad.*,paqueteStateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>

<%!
    private LibroBeanRemote librocat = null;
    String s1, s2, s3;
    Collection list;

    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando catalogo" + librocat);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>
<%
    try {
        s1 = request.getParameter("t1");
        s2 = request.getParameter("aut");
        s3 = request.getParameter("precio");
        if (s1 != null && s2 != null && s3 != null) {
            Double precio = new Double(s3);
            BigDecimal b = new BigDecimal(precio);
            librocat.addLibro(s1, s2, b);
            System.out.println("Registro a�adido: ");
%>
<p><b>Registro dado de alta</b></p>
<%
        }
        list = librocat.getAllLibros();
        for(Iterator iter=list.iterator(); iter.hasNext();){
        Libro elemento=(Libro)iter.next();
        %>
        <br/>
        <p>ID: <b><%= elemento.getId() %> </b></p>
        <p>Titulo: <b><%= elemento.getTitulo() %> </b></p>
        <p>Autor: <b> <%= elemento.getAutor() %></b></p>
        <p>Precio: <b> <%= elemento.getPrecio() %> </b></p>
        <%
        }
        response.flushBuffer();
        %>
            <br/><h3>Regresar a <a href="forma.jsp">forma</a></h3>
            <br/><h3>Regresar a <a href="index.html">inicio</a></h3>
        <%
    } catch (Exception b) {
        b.printStackTrace();
    }
%>
