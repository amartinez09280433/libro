<%@page import="paqueteEntidad.*,paqueteStateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
    private LibroBeanRemote librocat = null;
    String s0, s1, s2, s3;
    Libro libro;

    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando catalogo" + librocat);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>
<%
    try {
        s0 = request.getParameter("id");
        if (s0 != null) {
            Integer Id = new Integer(s0);
            libro=librocat.buscaLibro(Id.intValue());
            s1 = libro.getTitulo();
            s2 = libro.getAutor();
            Double p = libro.getPrecio().doubleValue();
            s3 = p.toString();
            System.out.println("Registro encontrado: ");
%>
<jsp:forward page="forma1.jsp">
    <jsp:param name="id" value="<%=s0%>"/>
    <jsp:param name="t1" value="<%=s1%>"/>
    <jsp:param name="aut" value="<%=s2%>"/>
    <jsp:param name="precio" value="<%=s3%>"/>
</jsp:forward>
<%
        }
    } catch (Exception b) {
        b.printStackTrace();
    }
%>